﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static bool EvaluateCondition(this EventCondition c)
    {
        STATS s = c.stat;
        int svalue = StarsManager.Instance.GetStat(s);
        switch (c.comparer)
        {
            case BOOL_OPERATOR.ALWAYS:
                return true;
            case BOOL_OPERATOR.LESS_THAN:
                return svalue < c.value;
            case BOOL_OPERATOR.EQUAL_TO:
                return svalue == c.value;
            case BOOL_OPERATOR.MORE_THAN:
                return svalue > c.value;
            case BOOL_OPERATOR.LESS_OR_EQUAL_TO:
                return svalue <= c.value;
            case BOOL_OPERATOR.MORE_OR_EQUAL_TO:
                return svalue >= c.value;
        }
        return false;
    }
}
