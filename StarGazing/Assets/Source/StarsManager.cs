﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct StatValues
{
    public STATS stat;
    public int value;
}


public class Node
{
    public List<Node> neighbours;
    public Transform transform;


    public Node(Transform t)
    {
        this.transform = t;
        this.neighbours = new List<Node>();
    }

    public Node(Transform t, List<Node> neighbours)
    {
        this.transform = t;
        this.neighbours = neighbours;
        foreach (var item in neighbours)
        {
            item.neighbours.Add(this);
        }
    }
}

public class StarsManager : MonoBehaviour
{
    public static StarsManager Instance;
    public List<StatValues> stats;
    public LineRenderer mLine;

    public Edge edgePrefab;
    public List<Edge> instantiatedEdges;

    public List<Transform> connectedStars;
    public Node lastNode;
    public Dictionary<Transform, Node> nodesByTransform;
    public List<Node> allNodes;

    public UnityEngine.UI.Text cycleSize3Label;
    public UnityEngine.UI.Text cycleSize4Label;
    public UnityEngine.UI.Text cycleSize5Label;
    public int cycleSize3Count;
    public int cycleSize4Count;
    public int cycleSize5Count;

    private void Awake()
    {
        Instance = this;
        nodesByTransform = new Dictionary<Transform, Node>();
        allNodes = new List<Node>();
    }

    public int GetStat(STATS stat)
    {
        return stats.Find(x => x.stat == stat).value;
    }

    public void AddStar(Star s)
    {
        Node newNode;
        if (nodesByTransform.TryGetValue(s.transform, out newNode))
        {
            MakeNeightbor(newNode,lastNode);            
        }
        else
        {
            if (lastNode != null)
            {
                newNode = new Node(s.transform);
                MakeNeightbor(newNode, lastNode);
                nodesByTransform.Add(s.transform,newNode);
                allNodes.Add(newNode);
            }
            else
            {
                newNode = new Node(s.transform);
                nodesByTransform.Add(s.transform, newNode);
                allNodes.Add(newNode);                
            }
        }

        lastNode = newNode;
        connectedStars.Add(s.transform);
        UpdateConnectedTransforms();
    }

    public void RemoveStar(Star s)
    {
        Node node;
        if(nodesByTransform.TryGetValue(s.transform, out node))
        {
            RemoveAllEdges(node);
            RemoveNode(node);
        }
    }

    public void AddLoneStar(Star s)
    {
        Node newNode;
        if (nodesByTransform.TryGetValue(s.transform, out newNode))
        {
            //Star already exists.
            return;
        }

      
        newNode = new Node(s.transform);
        nodesByTransform.Add(s.transform, newNode);
        allNodes.Add(newNode);

        
        connectedStars.Add(s.transform);
        UpdateConnectedTransforms();
    }

    public void AddStarPair(Star existingStar, Star newStar)
    {
        Node newNode;
        if (nodesByTransform.TryGetValue(newStar.transform, out newNode))
        {


        }
        else
        {
            newNode = new Node(newStar.transform);
            nodesByTransform.Add(newStar.transform, newNode);
            allNodes.Add(newNode);
            connectedStars.Add(newStar.transform);
        }

        MakeNeightbor(newNode, nodesByTransform[existingStar.transform]);        
        UpdateConnectedTransforms();
    }


    #region EDGES


    public void CreateEdge(Node a, Node b)
    {
        Edge e = Instantiate(edgePrefab) as Edge;
        e.gameObject.name = string.Format("Edge <{0}-{1}>", a.transform.name, b.transform.name);
        e.SetNodes(a, b);

        if (instantiatedEdges == null)
            instantiatedEdges = new List<Edge>();
        instantiatedEdges.Add(e);
    }

    public void RemoveEdge(Node a, Node b)
    {
        Edge e = FindEdge(a, b);
        instantiatedEdges.Remove(e);
        Destroy(e.gameObject);
    }

    public void RemoveAllEdges(Node a)
    {
        List<Edge> le = instantiatedEdges.FindAll(x => x.nodeA == a || x.nodeB == a);
        for (int i = 0; i < le.Count; i++)
        {
            instantiatedEdges.Remove(le[i]);
            Destroy(le[i].gameObject);
        }
    }

    public Edge FindEdge(Node a, Node b)
    {
        return instantiatedEdges.Find(x => (x.nodeA == a && x.nodeB == b) || (x.nodeB == a && x.nodeA == b));
    }

    #endregion

    public void MakeNeightbor(Node a, Node b)
    {
        a.neighbours.Add(b);
        b.neighbours.Add(a);
    }

    public void RemoveNode(Node n)
    {
        foreach (var item in n.neighbours)
        {
            item.neighbours.Remove(n);
        }
        nodesByTransform.Remove(n.transform);
        allNodes.Remove(n);
    }

    public void RemoveLast()
    {
        if (connectedStars.Count == 0)
            return;
        connectedStars.RemoveAt(connectedStars.Count-1);
        UpdateConnectedTransforms();
    }

    public Star GetLastStar()
    {
        if (connectedStars.Count == 0)
            return null;
        return connectedStars[connectedStars.Count-1].GetComponent<Star>();
    }


    public void UpdateConnectedTransforms()
    {
        int[,] g = CreateGraph(allNodes);

        DrawGraphEdges(g);

        Debug.Log(PrintGraph(g));
        cycleSize3Count = CountNodeGraphCycles(3);
        cycleSize3Label.text = cycleSize3Count.ToString();

        cycleSize4Count = CountNodeGraphCycles(4);
        cycleSize4Label.text = cycleSize4Count.ToString();

        cycleSize5Count = CountNodeGraphCycles(5);
        cycleSize5Label.text = cycleSize5Count.ToString();
    }

    void DrawGraphEdges(int[,] g)
    {
        for (int i = 0; i < allNodes.Count; i++)
        {
            for (int j = 0; j < allNodes.Count; j++)
            {
                if (i != j)
                {
                    Node a = allNodes[i];
                    Node b = allNodes[j];

                    Edge e = FindEdge(a, b);
                    if (e == null)
                    {
                        CreateEdge(a, b);
                    }
                    else if (!a.neighbours.Contains(b) && !b.neighbours.Contains(a))
                    {
                        RemoveEdge(a, b);
                    }
                }
            }
        }
    }


    /*
    public void SetConnectedTransforms(List<Transform> transforms)
    {        
        mLine.positionCount = transforms.Count;
        Vector3[] v = new Vector3[transforms.Count];
        for (int i = 0; i < transforms.Count; i++)
        {
            v[i] = transforms[i].position;
        }
        mLine.SetPositions(v);

        Debug.Log(PrintGraph(CreateGraph(allNodes)));
        cycleSize3Count = CountNodeGraphCycles(3);
        cycleSize3Label.text = cycleSize3Count.ToString();

        cycleSize4Count = CountNodeGraphCycles(4);
        cycleSize4Label.text = cycleSize4Count.ToString();

        cycleSize5Count = CountNodeGraphCycles(5);
        cycleSize5Label.text = cycleSize5Count.ToString();
    }*/


    #region GRAPH

    public int CountNodeGraphCycles(int size)
    {
        int[,] g = CreateGraph(allNodes);
        int r = countCycles(g, size);
        return r;
    }

    /// <summary>
    /// Returns a graph with connections between nodes, ordered by the list order
    /// </summary>
    /// <param name="l"></param>
    /// <returns></returns>
    public int[,] CreateGraph(List<Node> l)
    {
        int[,] graph = new int[l.Count,l.Count];

        for (int i = 0; i < l.Count; i++)
        {
            Node n = l[i];
            for (int j = 0; j < l.Count; j++)
            {
                Node other = l[j];
                graph[i, j] = (n.neighbours.Contains(other)) ? 1 : 0;
            }
        }

        return graph;
    }

    public void DFS(int[,] graph, bool[] marked, int n, int vert, int start, ref int count )
    {
        marked[vert] = true;
        if (n == 0)
        {
            marked[vert] = false;
            if (graph[vert, start] == 1)
            {
                count++;
                return;
            }
            else
                return;
        }
        for (int i = 0; i < GetNodeCount(); i++)
        {
            if (!marked[i] && graph[vert,i]==1)
            {
                DFS(graph, marked, n - 1, i, start, ref count);
            }
        }
        marked[vert] = false;
    }

    // Counts cycles of length N in an undirected
    // and connected graph.
    int countCycles(int[,] graph, int n)
    {
        // all vertex are marked un-visited intially.
        bool[] marked = new bool[GetNodeCount()];
        for (int i = 0; i < marked.Length; i++) { marked[i] = false; }
        
        // Searching for cycle by using v-n+1 vertices
        int count = 0;
        for (int i = 0; i < GetNodeCount() - (n - 1); i++)
        {
            DFS(graph, marked, n - 1, i, i, ref count);

            // ith vertex is marked as visited and
            // will not be visited again.
            marked[i] = true;
        }

        return count / 2;
    }

    public int GetNodeCount()
    {
        return allNodes.Count;
    }

    public string PrintGraph(int[,] g)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        for (int i = 0; i < g.GetLength(0); i++)
        {
            sb.Append(i+":(");
            for (int j = 0; j < g.GetLength(1); j++)
            {
                sb.Append(g[i,j].ToString());
            }
            sb.Append(")\n");
        }
        return sb.ToString();
    }
    #endregion
}





