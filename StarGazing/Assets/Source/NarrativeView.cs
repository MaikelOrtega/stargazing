﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Text;
using System;

public enum NARRATIVE_STATES
{
    WRITING,
    BLOCK,
    WAITING_CHOICE,
    WAITING_CLICK,
}

public class NarrativeView : MonoBehaviour
{
    public NARRATIVE_STATES state;
    public Text uiText;
    public float charsPerSecond = 0.02f;
    private string nextText;

    public Button choiceBtnA;
    private Text choiceTxtA;
    public Button choiceBtnB;
    private Text choiceTxtB;

    private UnityAction BtnAListener;
    private UnityAction BtnBListener;

    EventChoice currentChoiceA;
    EventChoice currentChoiceB;


    public void Start()
    {
        NarrativeManager.Instance.OnEventLoaded += OnEventLoaded;
        choiceTxtA = choiceBtnA.GetComponentInChildren<Text>();
        choiceTxtB = choiceBtnB.GetComponentInChildren<Text>();
        BtnAListener = () => OnBtnA();
        BtnBListener = () => OnBtnB();
    }

    

    public void Update()
    {
        switch (state)
        {
            case NARRATIVE_STATES.WRITING:

                break;
            case NARRATIVE_STATES.WAITING_CHOICE:
                CheckForChoice();
                break;
            case NARRATIVE_STATES.WAITING_CLICK:
                CheckForClick();
                break;
            case NARRATIVE_STATES.BLOCK:
                break;
        }
    }

    private void OnEventLoaded(ScriptableEvent ev, string txt)
    {
        nextText = txt;
        ChangeToWriting(ChangeToWaitForChoice);
    }

    void CheckForChoice()
    {
        
    }

    void CheckForClick()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            state = NARRATIVE_STATES.BLOCK;
            NarrativeManager.Instance.NextEvent();
        }
    }

    void ChangeToWriting(System.Action callback)
    {
        state = NARRATIVE_STATES.WRITING;
        Regex regex = new Regex("\\S");
        uiText.text = regex.Replace(nextText, " ");
        StartCoroutine(FillText(nextText, callback));
    }

    void ChangeToWaitForClick()
    {
        state = NARRATIVE_STATES.WAITING_CLICK;
    }

    void ChangeToWaitForChoice()
    {
        state = NARRATIVE_STATES.WAITING_CHOICE;
        currentChoiceA = NarrativeManager.Instance.currentEvent.choiceA;
        currentChoiceB = NarrativeManager.Instance.currentEvent.choiceB;
        SetButtonsText(currentChoiceA.text, currentChoiceB.text);        
        SetButtons(true);
        SubscribeToButtons();
    }

    void SetButtonsText(string a, string b)
    {
        choiceTxtA.text = a;
        choiceTxtB.text = b;
    }

    private void UnsubscribeButtons()
    {
        choiceBtnA.onClick.RemoveListener(BtnAListener);
        choiceBtnB.onClick.RemoveListener(BtnBListener);
    }

    private void SubscribeToButtons()
    {
        choiceBtnA.onClick.AddListener(BtnAListener);
        choiceBtnB.onClick.AddListener(BtnBListener);
    }

    void OnBtnA()
    {
        NarrativeManager.Instance.SolveChoice(currentChoiceA);
        nextText = currentChoiceA.response;
        AfterChoice();
    }

    void OnBtnB()
    {
        NarrativeManager.Instance.SolveChoice(currentChoiceB);
        nextText = currentChoiceB.response;
        AfterChoice();
    }

    void AfterChoice()
    {
        SetButtons(false);
        UnsubscribeButtons();
        ChangeToWriting(ChangeToWaitForClick);
    }

    private void SetButtons(bool v)
    {
        choiceBtnA.gameObject.SetActive(v);
        choiceBtnB.gameObject.SetActive(v);
    }

    IEnumerator FillText(string txt, System.Action callback=null)
    {
        int counter = 0;
        while (counter < txt.Length)
        {
            StringBuilder sb = new StringBuilder(uiText.text);
            sb[counter]= txt[counter];
            uiText.text = sb.ToString();
            counter++;
            yield return new WaitForSeconds(charsPerSecond);
        }
        if(callback != null)
            callback.Invoke();
    }
}