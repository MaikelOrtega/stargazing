﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    Camera cam;
    Star currentStar;
    Star firstStar;    


    private void Awake()
    {
        cam = this.GetComponent<Camera>();
    }

    void Update ()
    {
        CheckUnderView();
        if (Input.GetMouseButtonDown(0) && currentStar != null)
        {
            //TryToSelect();
            PairSelection();
        }
        else if (Input.GetMouseButtonDown(1) && currentStar != null)
        {
            RemoveSingleStar();
        }
    }

    void RemoveSingleStar()
    {
        StarsManager.Instance.RemoveStar(currentStar);        
    }

    void TryToSelect()
    {
        if (StarsManager.Instance.GetLastStar() == currentStar)
        {
            StarsManager.Instance.RemoveLast();
            currentStar.OnSelection();
        }
        else
        {
            StarsManager.Instance.AddStar(currentStar);            
            currentStar.OnSelection();
        }        
    }

    void PairSelection()
    {
        if (firstStar == null)
        {
            firstStar = currentStar;
            StarsManager.Instance.AddLoneStar(currentStar);
        }
        else
        {
            StarsManager.Instance.AddStarPair(firstStar, currentStar);
            firstStar = null;
        }
    }

    void CheckUnderView()
    {
        Ray r = cam.ViewportPointToRay(new Vector2(0.5f, 0.5f));
        RaycastHit info;
        if (Physics.Raycast(r, out info))
        {
            Star s = info.collider.GetComponent<Star>();
            if (s != null)
            {
                if (s != currentStar)
                {
                    if (currentStar != null)
                    {
                        currentStar.OnDeselect();
                    }
                    currentStar = s;
                    currentStar.OnRaycastHit();
                }
            }
        }
        else
        {
            if (currentStar != null)
            {
                currentStar.OnDeselect();
                currentStar = null;
            }
        }
    }
}

