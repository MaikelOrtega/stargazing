﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge : MonoBehaviour
{
    public LineRenderer mLine;
    public Node nodeA;
    public Node nodeB;


    public void SetNodes(Node a, Node b)
    {
        nodeA = a;
        nodeB = b;
        mLine.positionCount = 2;
        mLine.SetPositions(new Vector3[]{a.transform.position,b.transform.position});
    }

    public override bool Equals(object other)
    {
        Edge e = (other as Edge);

        return (
            (e.nodeA == this.nodeB && e.nodeB == this.nodeA) ||
            (e.nodeA == this.nodeA && e.nodeB == this.nodeB));        
    }
}
