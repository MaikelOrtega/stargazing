﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Event List", menuName = "Create event list")]
public class ScriptableEventList : ScriptableObject
{
    public List<ScriptableEvent> eventList;
}
