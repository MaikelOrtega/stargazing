﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeManager : MonoBehaviour
{
    static public NarrativeManager Instance;

    [Header("TEST ONLY")]
    public ScriptableEvent testEvent;
    public ScriptableEventList testList;

    public List<ScriptableEvent> currentList;
    public ScriptableEvent currentEvent;
    public int eventIndex=0;

    ScriptableEvent[] aux;

    public delegate void NarrativeEventDelegate(ScriptableEvent ev, string txt);
    public event NarrativeEventDelegate OnEventLoaded;

    private void Awake()
    {
        Instance = this;
        aux = new ScriptableEvent[testList.eventList.Count];
        testList.eventList.CopyTo(aux);
        currentList = new List<ScriptableEvent>(aux);
    }

    void LoadEvent(ScriptableEvent e)
    {
        string txt = string.Format("{0}: {1}\n{2}\n{3}",e.character.ToString(),e.text,e.choiceA.text,e.choiceB.text);
        currentEvent = e;
        Debug.Log(txt);
        txt = string.Format("{0}: {1}", e.character.ToString(), e.text);
        if (OnEventLoaded != null)
        {
            OnEventLoaded(e, txt);
        }
    }

    private void Start()
    {
        Init();
    }

    void Init()
    {
        currentEvent = testList.eventList[0];
        LoadEvent(currentEvent);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            LoadEvent(testEvent);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            NextEvent();
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            SolveChoice(currentEvent.choiceA);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            SolveChoice(currentEvent.choiceB);
        }
    }

    public void NextEvent()
    {
        eventIndex++;
        if (eventIndex < currentList.Count)
        {
            LoadEvent(currentList[eventIndex]);
        }
    }

    void InsertEvent(ScriptableEvent e, int delay)
    {
        this.currentList.Insert(eventIndex + delay, e);
    }

    public void SolveChoice(EventChoice choice)
    {
        Debug.Log(">" + choice.text);
        Debug.Log(choice.response);

        bool res = choice.condition.EvaluateCondition();
        if (res)
        {
            Debug.Log("Success!");
            if (choice.successEvent != null)
            {
                InsertEvent(choice.successEvent, choice.turnsDelay);
                Debug.Log(string.Format("Inserting {0} in {1} turns", choice.successEvent.name, choice.turnsDelay));
            }
        }
        else
        {
            Debug.Log("Fail!");
            Debug.Log(string.Format("Inserting {0} in {1} turns", choice.failEvent.name, choice.turnsDelay));

            InsertEvent(choice.failEvent, choice.turnsDelay);
        }        
    }    
}
