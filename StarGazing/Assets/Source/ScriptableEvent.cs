﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EVENT_CHARACTERS
{
    HORSE,
    BULL,
    REINDEER
}

public enum STATS
{
    ATTACK,
    DEFENSE
}

public enum BOOL_OPERATOR
{
    ALWAYS,
    LESS_THAN,
    LESS_OR_EQUAL_TO,
    EQUAL_TO,
    MORE_OR_EQUAL_TO,
    MORE_THAN,    
}


[System.Serializable]
public struct EventCondition
{
    public STATS stat;
    public BOOL_OPERATOR comparer;
    public int value;
}

[System.Serializable]
public struct EventChoice
{
    public string text;
    public string response;
    public EventCondition condition;
    public ScriptableEvent successEvent;
    public ScriptableEvent failEvent;
    public int turnsDelay;
}

[CreateAssetMenu(fileName = "New Binary Event", menuName = "Create new binary event")]
public class ScriptableEvent : ScriptableObject
{
    public EVENT_CHARACTERS character;
    public string text;
    public EventChoice choiceA;
    public EventChoice choiceB;
}
