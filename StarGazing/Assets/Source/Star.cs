﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Star : MonoBehaviour
{
    public void OnRaycastHit()
    {
        Debug.Log("RAY HIT");
        transform.DOPunchScale(Vector3.one * 1.1f, 0.25f);
    }

    public void OnDeselect()
    {

    }

    public void OnSelection()
    {
        Debug.Log("SELECTED");
        transform.DOPunchScale(Vector3.one * 2.1f, 0.25f);
    }
}
