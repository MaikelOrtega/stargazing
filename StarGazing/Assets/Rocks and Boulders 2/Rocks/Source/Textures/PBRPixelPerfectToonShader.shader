// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:True,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1873,x:34012,y:32838,varname:node_1873,prsc:2|normal-1413-RGB,custl-2166-OUT,clip-1832-A,olwid-2444-OUT;n:type:ShaderForge.SFN_Tex2d,id:1832,x:32620,y:32686,ptovrint:False,ptlb:_MainTex,ptin:__MainTex,varname:node_1832,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:False|UVIN-3451-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:1413,x:33551,y:32661,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_1413,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True|UVIN-3451-UVOUT;n:type:ShaderForge.SFN_Posterize,id:3610,x:32620,y:33021,varname:node_3610,prsc:2|IN-6136-OUT,STPS-2920-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2920,x:32373,y:33137,ptovrint:False,ptlb:Steps,ptin:_Steps,varname:node_2920,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:4;n:type:ShaderForge.SFN_Tex2d,id:8079,x:32999,y:32982,ptovrint:False,ptlb:Metalness,ptin:_Metalness,varname:node_8079,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-3451-UVOUT;n:type:ShaderForge.SFN_Multiply,id:8288,x:32999,y:32797,varname:node_8288,prsc:2|A-2424-OUT,B-4537-OUT;n:type:ShaderForge.SFN_TexCoord,id:3451,x:32452,y:32686,varname:node_3451,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Color,id:9238,x:32620,y:32869,ptovrint:False,ptlb:Diffuse Color,ptin:_DiffuseColor,varname:node_9238,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:2424,x:32802,y:32797,varname:node_2424,prsc:2|A-1832-RGB,B-9238-RGB;n:type:ShaderForge.SFN_Dot,id:6136,x:32373,y:32969,varname:node_6136,prsc:2,dt:1|A-6363-OUT,B-5132-OUT;n:type:ShaderForge.SFN_LightVector,id:6363,x:31974,y:32895,varname:node_6363,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:5132,x:31974,y:33028,prsc:2,pt:True;n:type:ShaderForge.SFN_LightColor,id:6996,x:33551,y:32952,varname:node_6996,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2166,x:33761,y:32952,varname:node_2166,prsc:2|A-6996-RGB,B-9841-OUT,C-9058-OUT;n:type:ShaderForge.SFN_Slider,id:6504,x:31455,y:33361,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_6504,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Vector1,id:8900,x:31612,y:33439,varname:node_8900,prsc:2,v1:10;n:type:ShaderForge.SFN_Multiply,id:5673,x:31811,y:33358,varname:node_5673,prsc:2|A-6504-OUT,B-8900-OUT;n:type:ShaderForge.SFN_Vector1,id:6175,x:31811,y:33513,varname:node_6175,prsc:2,v1:1;n:type:ShaderForge.SFN_Add,id:192,x:31988,y:33358,varname:node_192,prsc:2|A-5673-OUT,B-6175-OUT;n:type:ShaderForge.SFN_Exp,id:7983,x:32164,y:33358,varname:node_7983,prsc:2,et:1|IN-192-OUT;n:type:ShaderForge.SFN_HalfVector,id:1730,x:31974,y:33195,varname:node_1730,prsc:2;n:type:ShaderForge.SFN_Dot,id:4909,x:32164,y:33195,varname:node_4909,prsc:2,dt:2|A-5132-OUT,B-1730-OUT;n:type:ShaderForge.SFN_Power,id:1966,x:32373,y:33195,varname:node_1966,prsc:2|VAL-4909-OUT,EXP-7983-OUT;n:type:ShaderForge.SFN_Posterize,id:8238,x:32620,y:33195,varname:node_8238,prsc:2|IN-1966-OUT,STPS-2920-OUT;n:type:ShaderForge.SFN_Clamp01,id:1916,x:32797,y:33195,varname:node_1916,prsc:2|IN-8238-OUT;n:type:ShaderForge.SFN_Clamp01,id:4537,x:32797,y:33021,varname:node_4537,prsc:2|IN-3610-OUT;n:type:ShaderForge.SFN_Multiply,id:8264,x:33187,y:33081,varname:node_8264,prsc:2|A-8079-RGB,B-1916-OUT;n:type:ShaderForge.SFN_AmbientLight,id:890,x:32620,y:33339,varname:node_890,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:851,x:32620,y:33495,ptovrint:False,ptlb:Ambient Occlusion,ptin:_AmbientOcclusion,varname:node_851,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-3451-UVOUT;n:type:ShaderForge.SFN_Multiply,id:6056,x:32808,y:33339,varname:node_6056,prsc:2|A-890-RGB,B-851-RGB;n:type:ShaderForge.SFN_LightAttenuation,id:9058,x:33551,y:32826,varname:node_9058,prsc:2;n:type:ShaderForge.SFN_Add,id:9841,x:33383,y:33060,varname:node_9841,prsc:2|A-8288-OUT,B-8264-OUT,C-1782-OUT;n:type:ShaderForge.SFN_Multiply,id:1782,x:33187,y:33334,varname:node_1782,prsc:2|A-6056-OUT,B-5216-OUT;n:type:ShaderForge.SFN_Slider,id:5216,x:32785,y:33525,ptovrint:False,ptlb:AO Intensity,ptin:_AOIntensity,varname:node_5216,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_ValueProperty,id:2444,x:33780,y:33215,ptovrint:False,ptlb:node_2444,ptin:_node_2444,varname:node_2444,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;proporder:1413-2920-1832-8079-9238-6504-851-5216-2444;pass:END;sub:END;*/

Shader "Shader Forge/PBRPixelPerfectToonShader" {
    Properties {
        _Normal ("Normal", 2D) = "bump" {}
        _Steps ("Steps", Float ) = 4
        __MainTex ("_MainTex", 2D) = "bump" {}
        _Metalness ("Metalness", 2D) = "white" {}
        _DiffuseColor ("Diffuse Color", Color) = (0.5,0.5,0.5,1)
        _Gloss ("Gloss", Range(0, 1)) = 1
        _AmbientOcclusion ("Ambient Occlusion", 2D) = "white" {}
        _AOIntensity ("AO Intensity", Range(0, 1)) = 0.5
        _node_2444 ("node_2444", Float ) = 0.1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D __MainTex; uniform float4 __MainTex_ST;
            uniform float _node_2444;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*_node_2444,1) );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 __MainTex_var = tex2D(__MainTex,TRANSFORM_TEX(i.uv0, __MainTex));
                clip(__MainTex_var.a - 0.5);
                return fixed4(float3(0,0,0),0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D __MainTex; uniform float4 __MainTex_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _Steps;
            uniform sampler2D _Metalness; uniform float4 _Metalness_ST;
            uniform float4 _DiffuseColor;
            uniform float _Gloss;
            uniform sampler2D _AmbientOcclusion; uniform float4 _AmbientOcclusion_ST;
            uniform float _AOIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float4 __MainTex_var = tex2D(__MainTex,TRANSFORM_TEX(i.uv0, __MainTex));
                clip(__MainTex_var.a - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Metalness_var = tex2D(_Metalness,TRANSFORM_TEX(i.uv0, _Metalness));
                float4 _AmbientOcclusion_var = tex2D(_AmbientOcclusion,TRANSFORM_TEX(i.uv0, _AmbientOcclusion));
                float3 finalColor = (_LightColor0.rgb*(((__MainTex_var.rgb*_DiffuseColor.rgb)*saturate(floor(max(0,dot(lightDirection,normalDirection)) * _Steps) / (_Steps - 1)))+(_Metalness_var.rgb*saturate(floor(pow(min(0,dot(normalDirection,halfDirection)),exp2(((_Gloss*10.0)+1.0))) * _Steps) / (_Steps - 1)))+((UNITY_LIGHTMODEL_AMBIENT.rgb*_AmbientOcclusion_var.rgb)*_AOIntensity))*attenuation);
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D __MainTex; uniform float4 __MainTex_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _Steps;
            uniform sampler2D _Metalness; uniform float4 _Metalness_ST;
            uniform float4 _DiffuseColor;
            uniform float _Gloss;
            uniform sampler2D _AmbientOcclusion; uniform float4 _AmbientOcclusion_ST;
            uniform float _AOIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float4 __MainTex_var = tex2D(__MainTex,TRANSFORM_TEX(i.uv0, __MainTex));
                clip(__MainTex_var.a - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Metalness_var = tex2D(_Metalness,TRANSFORM_TEX(i.uv0, _Metalness));
                float4 _AmbientOcclusion_var = tex2D(_AmbientOcclusion,TRANSFORM_TEX(i.uv0, _AmbientOcclusion));
                float3 finalColor = (_LightColor0.rgb*(((__MainTex_var.rgb*_DiffuseColor.rgb)*saturate(floor(max(0,dot(lightDirection,normalDirection)) * _Steps) / (_Steps - 1)))+(_Metalness_var.rgb*saturate(floor(pow(min(0,dot(normalDirection,halfDirection)),exp2(((_Gloss*10.0)+1.0))) * _Steps) / (_Steps - 1)))+((UNITY_LIGHTMODEL_AMBIENT.rgb*_AmbientOcclusion_var.rgb)*_AOIntensity))*attenuation);
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D __MainTex; uniform float4 __MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 __MainTex_var = tex2D(__MainTex,TRANSFORM_TEX(i.uv0, __MainTex));
                clip(__MainTex_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
